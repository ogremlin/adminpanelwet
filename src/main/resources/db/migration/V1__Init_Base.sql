create table roles
(
    id   bigint not null auto_increment,
    role varchar(255),
    primary key (id)
);

create table users
(
    id       bigint not null auto_increment,
    email    varchar(255),
    password varchar(255),
    username varchar(255),
    primary key (id)
);

create table users_roles
(
    user_id  bigint not null,
    roles_id bigint not null,
    primary key (user_id, roles_id)
);

alter table users_roles add constraint FK_USERS_ON_ROLES foreign key (roles_id) references roles (id);
alter table users_roles add constraint FK_ROLES_ON_USERS foreign key (user_id) references users (id);
--
-- ALTER TABLE answer
--     ADD CONSTRAINT FK_ANSWER_ON_QUESTION FOREIGN KEY (question_id) REFERENCES question (id);
--
-- ALTER TABLE answer
--     ADD CONSTRAINT FK_ANSWER_ON_USER FOREIGN KEY (user_id) REFERENCES user_entity (id);